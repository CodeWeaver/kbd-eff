#!/usr/bin/python3

from collections import defaultdict
from sys import argv


# Automatically initialize keys with 0
chars = defaultdict(int)
pairs = defaultdict(int)

for file in argv[1:]:
    with open(file, 'r') as corpus:
            prev = ' '
            char = ' '
            while True:
                    prev = char
                    char = corpus.read(1).upper()   # Read corpus character-by-character
                    if not char: break;     # End of stream
                    
                    # Count up non-whitespace characters
                    if char.isspace(): continue
                    chars[char] += 1
                    
                    # Count character-pairs that don't include whitespace or the same character twice
                    if prev.isspace() or prev == char: continue
                    pairs[frozenset([prev,char])] += 1


if __name__ == '__main__':
        print("Character Counts:")
        for char,count in sorted(chars.items(), key=lambda c: c[1], reverse=True): print(char, "\t", count)
        print("\n\nCharacter-Pair Counts:")
        for pair,count in sorted(pairs.items(), key=lambda c: c[1], reverse=True): print(pair, "\t", count)
