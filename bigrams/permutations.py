#!/usr/bin/python3

from count_grams import chars as char_counts
from count_grams import pairs
from itertools import combinations, permutations
from math import inf


# This part can be modified!
# Here we are assuming that we have eight groups (one per finger),
# each with `size` number of keys and an average effort of `weight`
groups = [
    {'size': 3, 'weight': 4/3},    # left pinky
    {'size': 3, 'weight': 1},      # left ring
    {'size': 3, 'weight': 1},      # left middle
    {'size': 6, 'weight': 11/6},   # left index
    {'size': 6, 'weight': 2},      # right index
    {'size': 3, 'weight': 1},      # right middle
    {'size': 3, 'weight': 1},      # right ring
    {'size': 5, 'weight': 7/5}     # right pinky
]

# How many keys are we organizing?
keys = 0
for g in groups: keys += g['size']

# Get the most frequent characters that fit our number of keys
chars = [char for char,_ in sorted(char_counts.items(), key=lambda c: c[1], reverse=True)][0:keys]

# Initialize group assignments
assigns = []
# Here we assign group `i` to `g['size']` characters
for i,g in enumerate(groups):
    for k in range(g['size']):
        assigns.append(i)


# Evaluate every possible organization, keep track of the one(s) with the best score
winners = set()
best = inf
for assignment in permutations(assigns, keys):   # Getting every permutation of assigning each character a group number
    score = 0
    groupings = [set() for _ in range(len(groups))]
    for char_index,group_index in enumerate(assignment):
        groupings[group_index].add(chars[char_index])
    for group_index,set_ in enumerate(groupings):
        for pair in combinations(set_, 2):
            score += pairs[frozenset(pair)] * groups[group_index]['weight']
    if score == best:
        winners.add(assignment)
    elif score < best:
        best = score
        winners = {assignment}

print(best)
print(chars)
print(winners)