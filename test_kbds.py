#!/usr/bin/python3
# `test_kbds.py` -- Keyboard Layout Efficiency Tester
# by Logan "CodeWeaver" Hall
# Requires Python 3.5 or higher!
from enum import Enum
from os import listdir
import fileinput


class OrderedEnum(Enum):
    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented
    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented
    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

class Hand(OrderedEnum):
    left = 0
    right = 1
    either = 2

class Finger(OrderedEnum):
    thumb = 0
    index = 1
    middle = 2
    ring = 3
    pinky = 4
    extra = 5


master = [
    { # `
        'x': -1,
        'y': 5,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # 1
        'x': 0,
        'y': 5,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # 2
        'x': 0,
        'y': 3,
        'h': Hand.left,
        'f': Finger.ring
    },
    { # 3
        'x': 0,
        'y': 3,
        'h': Hand.left,
        'f': Finger.middle
    },
    { # 4
        'x': 0,
        'y': 4,
        'h': Hand.left,
        'f': Finger.index
    },
    { # 5
        'x': 0,
        'y': 4,
        'h': Hand.left,
        'f': Finger.index
    },
    { # 6
        'x': 2,
        'y': 4,
        'h': Hand.left,
        'f': Finger.index
    },
    { # 7
        'x': 0,
        'y': 5,
        'h': Hand.right,
        'f': Finger.index
    },
    { # 8
        'x': 0,
        'y': 3,
        'h': Hand.right,
        'f': Finger.middle
    },
    { # 9
        'x': 0,
        'y': 4,
        'h': Hand.right,
        'f': Finger.ring
    },
    { # 0
        'x': 0,
        'y': 5,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # -
        'x': 0,
        'y': 4,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # =
        'x': 1,
        'y': 4,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # q
        'x': 0,
        'y': 2,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # w
        'x': 0,
        'y': 1,
        'h': Hand.left,
        'f': Finger.ring
    },
    { # e
        'x': 0,
        'y': 1,
        'h': Hand.left,
        'f': Finger.middle
    },
    { # r
        'x': 0,
        'y': 2,
        'h': Hand.left,
        'f': Finger.index
    },
    { # t
        'x': 1,
        'y': 2,
        'h': Hand.left,
        'f': Finger.index
    },
    { # y
        'x': -2,
        'y': 3,
        'h': Hand.right,
        'f': Finger.index
    },
    { # u
        'x': 0,
        'y': 3,
        'h': Hand.right,
        'f': Finger.index
    },
    { # i
        'x': 0,
        'y': 1,
        'h': Hand.right,
        'f': Finger.middle
    },
    { # o
        'x': 0,
        'y': 1,
        'h': Hand.right,
        'f': Finger.ring
    },
    { # p
        'x': 0,
        'y': 2,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # [
        'x': 1,
        'y': 1,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # ]
        'x': 2,
        'y': 1,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # \
        'x': 4,
        'y': 1,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # a
        'x': 0,
        'y': 0,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # s
        'x': 0,
        'y': 0,
        'h': Hand.left,
        'f': Finger.ring
    },
    { # d
        'x': 0,
        'y': 0,
        'h': Hand.left,
        'f': Finger.middle
    },
    { # f
        'x': 0,
        'y': 0,
        'h': Hand.left,
        'f': Finger.index
    },
    { # g
        'x': 2,
        'y': 0,
        'h': Hand.left,
        'f': Finger.index
    },
    { # h
        'x': -2,
        'y': 0,
        'h': Hand.right,
        'f': Finger.index
    },
    { # j
        'x': 0,
        'y': 0,
        'h': Hand.right,
        'f': Finger.index
    },
    { # k
        'x': 0,
        'y': 0,
        'h': Hand.right,
        'f': Finger.middle
    },
    { # l
        'x': 0,
        'y': 0,
        'h': Hand.right,
        'f': Finger.ring
    },
    { # ;
        'x': 0,
        'y': 0,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # '
        'x': 1,
        'y': 0,
        'h': Hand.right,
        'f': Finger.pinky
    },
    { # ISO
        'x': 0,
        'y': -1,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # z
        'x': 0,
        'y': -2,
        'h': Hand.left,
        'f': Finger.pinky
    },
    { # x
        'x': 0,
        'y': -2,
        'h': Hand.left,
        'f': Finger.ring
    },
    { # c
        'x': 0,
        'y': -2,
        'h': Hand.left,
        'f': Finger.middle
    },
    { # v
        'x': 0,
        'y': -1,
        'h': Hand.left,
        'f': Finger.index
    },
    { # b
        'x': 2,
        'y': -1,
        'h': Hand.left,
        'f': Finger.index
    },
    { # n
        'x': 0,
        'y': -1,
        'h': Hand.right,
        'f': Finger.index
    },
    { # m
        'x': 0,
        'y': -1,
        'h': Hand.right,
        'f': Finger.index
    },
    { # ,
        'x': 0,
        'y': -2,
        'h': Hand.right,
        'f': Finger.middle
    },
    { # .
        'x': 0,
        'y': -2,
        'h': Hand.right,
        'f': Finger.ring
    },
    { # /
        'x': 0,
        'y': -2,
        'h': Hand.right,
        'f': Finger.pinky
    }
] # master


# Initialize keyboards from directory
kbds = {}
KBD_LEN = 48    # Number of core keys in keyboard

for kbd in listdir('kbds/'):
    if kbd.startswith('.'):
        continue
    with open('kbds/'+kbd, 'r', encoding="ISO-8859-1") as keyboard:
        k = keyboard.read()
        
        # Carefully remove formatting whitespace
        k = k.replace("\r",'').replace("\n",'').replace(' ','',2)
        # Must not remove the extra ISO key, even if it's blank
        k = k[:KBD_LEN] + k[KBD_LEN:].replace(' ','',2)

        # Error-checking
        if len(k) != 96:
            print(len(k))
            raise(IOError(kbd + ' is malformed.'))

        # Get name of layout, initialize
        layout = kbd.replace('.txt','')
        kbds[layout] = { }
        # Set (unshifted) key positions for this layout
        for i in range(KBD_LEN):
            kbds[layout][k[i]] = {**master[i], 's':0}
        # Set up shift table for shifted keys
        for i in range(KBD_LEN):
            kbds[layout][k[i+KBD_LEN]] = {**master[i], 's':1}


score = {kbd: 0 for kbd in kbds.keys()}
wins = {kbd: 0 for kbd in kbds.keys()} # First place scores (lowest)


log = open("eff_test.log", 'w')
words = 0
for line in fileinput.input(openhook=fileinput.hook_encoded("latin-1")):
    for word in line.split():
        words += 1

        # Dict for storing scores for this word
        scores = {kbd: 0 for kbd in kbds.keys()}

        # Compute score for each layout
        try:
            for layout in kbds.keys():
    #           print(layout, line.rstrip())
                hand = {
                    'x': 0,
                    'y': 0,
                    'h': None,
                    'f': None,
                    's': 0
                }
                for char in list(word.rstrip()):
                    # Get hand position for this key, on this keyboard
                    move = kbds[layout][char]

                    ### Get base score from X & Y movements, relative to previous hand position
                    # No penalty for returning to home row
                    if move['x'] == 0 and move['y'] == 0:
                        val = 0
                    # Track distance between last hand position and this position
                    elif move['h'] == hand['h']:
                        x = move['x'] - hand['x']
                        y = move['y'] - hand['y']
                        # Acknowledge preemptive movement if lateral movements on same side of home position
                        if move['x'] ^ hand['x'] >= 0 and abs(move['x']) < abs(hand['x']): x = 0    # No successive movement occurred
                        # Acknowledge preemptive movement if verical movements on same row from home position
                        if move['y'] ^ hand['y'] >= 0 and abs(move['y']) < abs(hand['y']): y = 0    # No successive movement occurred
                        val = abs(x) + abs(y)
                    # Assume moving from home row if alternating hands
                    else:
                        val = abs(move['x']) + abs(move['y'])


                    if move['h'] == hand['h']:
                        # Penalty if finger is same between previous & current keystroke
                        if move['f'] == hand['f']: val += 3
                        # EXPERIMENTAL: Penalty if hand is same between previous & current keystroke
                        #val += 2
                        # EXPERIMENTAL: Penalize same-hand rolls
                        # Penalize inner-rolls
                        #if move['f'] < hand['f']: val += 1
                        # Penalize outer-rolls
                        #if move['f'] > hand['f']: val += 1


                    # Penalty if shift key pressed
                    if move['s'] and (not hand['s'] or move['h'] != hand['h']): val += move['s']

                    # Record this movement
                    hand = move

                    # Record this score
                    scores[layout] += val
        # Ignore glyphs that don't appear on the keyboard
        except KeyError as e:
            continue

        # Sort scores, declare winners, update data
        ranks = sorted(set(scores.values()))
        best = ranks[0]
        for kbd,result in scores.items():
            score[kbd] += result
            if result == best: wins[kbd] += 1
        log_entry = ''
        for kbd in scores.keys():
            log_entry += "{:<16}".format(kbd)
        log.write(word.rstrip() + "\n" + log_entry + "\n")
        log_entry = ''
        for result in scores.values():
            log_entry += "{:<16}".format(result)
        log.write(log_entry + "\n\n")
log.close()


# Log value of each letter in order of letter frequency
freqs = ['e','t','a','o','i','n','s','r','h','l','d','c','u','m','f','p','g','w','y','b',',','.','v','k','x','j','q','z']
freq_txt = open("frequencies.log", 'w')
freq_txt.write("{:<16}".format("Letter freq.") + ' '.join(freqs) + '\n')

# Display results
print("LAYOUT          SCORE    WINS")
print("=============================")
for kbd,result in sorted(list(score.items()), key=lambda x: x[1]):
    # Log letter values
    freq_txt.write("{:<16}".format(kbd))
    values = [abs(kbds[kbd][letter]['x']) + abs(kbds[kbd][letter]['y']) for letter in freqs]
    freq_txt.write(' '.join(str(val) for val in values) + '\n')
    
    # Print score & wins for this keyboard
    print("{:<16}".format(kbd) + "{:<8}".format(result) + "{:>5.1%}   ".format(float(wins[kbd]/words)))
